#!/bin/sh

version=11.0

wget -q --show-progress https://github.com/phracker/MacOSX-SDKs/releases/download/11.3/MacOSX$version.sdk.tar.xz
tar xf MacOSX$version.sdk.tar.xz
mkdir -p ~/.mozbuild/macos-sdk
cp -aH MacOSX$version.sdk ~/.mozbuild/macos-sdk/
