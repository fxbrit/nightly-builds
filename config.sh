#!/bin/sh

cp mozconfig mozilla-unified/mozconfig
echo "ac_add_options --with-macos-sdk=$HOME/.mozbuild/macos-sdk/MacOSX11.0.sdk" >> mozilla-unified/mozconfig
echo "ac_add_options --enable-bundled-fonts" >> mozilla-unified/mozconfig
