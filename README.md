## nightly-builds

testing good stuff with these scripts, osx only.

- `brew.sh` install mercurial and wget;
- `configh.sh` contains extra build configs I want to test;
- `mozboot.sh` bootstrap nightly;
- `sdk.sh` avoid xcode and fetch the sdk instead;
